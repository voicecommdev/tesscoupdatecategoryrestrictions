<?php
error_reporting(E_ALL);


$pdo = getDBConnection();
$table = 'myvoicecomm.users';

$user_cats_str = '175,258,295,362,384,397,407,422,423,425,446,447,463,464,479,502,521,544,578,616,622,628,634,640,643,644,656,659,669,676,687,705,706,710,714,715,730,731,735,736,737,751,752,767,773,774,775,791,796,797,798,803,804,807,808,810,812,813,814,816,818,819,820,827,829,830,831,832,836,837,838,839,840,841,844,850,851,861,865,866,867,868,870,872,873,875,876,879,881,882,885,900,901,911,912,913,921,922,927,930,931,932,934,940,941,943,944,945,946,947,949,950,956,959,960,963,964,968,969,970,975,980,981,983,988,991,992,993,995,997,1002,1003,1004,1005,1006,1007,1008,1009,1010,1011,1012,1013,1014,1016,1022,1023,1027,1032,1033,1034,1035,1043,1046,1047,1048,1051,1052,1053,1061,1062,1066,1067,1068,1069,1070,1072,1075,1078,1079,1083,1084,1087,1088,1089,1092,1093,1094,1095,1097,1098,1099,1100,1102,1103,1104,1105,1110,1115,1124,1125,1126,1127,1128,1129,1130,1131,1133,1134,1140,1147,1148,1149,1151,1152,1153,1154,1155,1157,1160,1164,1165,1167,1170,1171,1172,1173,1174,1180,1181,1182,1184,1185,1186,1195,1196,1200,1201,1202,1203,1204,1205,1207,1208,1209,1210,1211,1212,1213,1217,1219,1229,1231,1232,1233,1236,1238,1239,1240,1241,1242,1243,1244,1245,1248,1255,1256,1257,1258,1259,1261,1262,1263,1264,1265,1268,1269,1270,1272,1275,1276,1277,1278,1280,1281,1282,1283,1289,1290,1291,1292,1293,1294,1295,1297,1298,1299,1300,1306,1307,1308,1309,1310,1311,1312,1320,1321,1322,1323,1324,1325,1327,1328,1329,1330,1331,1332,1333,1334,1337,1339,1341,1342,1343,1344,1346,1349,1350,1351,1352,1354,1357,1358,1360,1362,1363,1364,1365,1366,1367,1368,1369,1370,1373,1374,1376,1379,1380,1381,1383,1384,1387,1388,1389,1391,1393,1394,1395,1396,1397,1402,1403,1404,1405,1406,1409,1410,1411,1412,1413,1414,1415,1416,1417,1418,1419';
// convert cats to array
$user_cats = explode(',', $user_cats_str);

$rows = array_map(function ($v) {
    return str_getcsv($v, "|");
}, file('TesscoRetailAccountBrandProtectionDetails-FULL.csv'));

// this just creates a smaller array to work with 
//$funcVC = function($row) {
//    return [$row[1], $row[0]];// pid => tid
//};

//$vc_tid_arr = array_map($funcVC, $rows);
//unset($vc_tid_arr[0]);
$header = array_shift($rows);

// Account	Account Name	Meta	Revenue Since 1.1.19	Authorized Brands	BODZ	LAND	GADG	GGB2	CMEC	RIFL	INCP	KSPD	GRFN	ORGN	SPCK	SONX	SPGN	POPS	NATU	UBIO	SATE	1MPH	ISOD	GR4B	UAGR	HIII	JABR	REBA	TEMP	SKUL	SCOS	JLAB
$master_cat_ids = ['bodz' => 182, 'land' =>207, 'gadg' => 41, 'ggb2' => '', 'cmec' => 50, 'rifl' => 224, 'incp' => 55, 'kspd' => 188, 'grfn' => 109, 'spck' => 48, 'sonx' => 101, 'spgn' => 113, 'pops' => 186, 'natu' => '', 'ubio' => '', 'sate' => '', '1mph' => 164, 'isod' => 184, 'gr4b' => 214, 'uagr' => 103, 'hiii' => 173, 'jabr' => 163, 'reba' => '', 'temp' => '', 'skul' => 156, 'scos' => 107, 'jlab' => ''];
$cat_ids = ['orgn' => 1334];

// now get the category ids for cats
$all_cat_ids = getCatIdsForMasterCats($master_cat_ids);

$non_cat_fields = ['account', 'accountname', 'meta', 'revenue', 'authorizedbrands'];
$cat_fields = ['bodz', 'land', 'gadg', 'ggb2', 'cmec', 'rifl', 'incp', 'kspd', 'grfn', 'orgn', 'spck', 'sonx', 'spgn', 'pops', 'natu', 'ubio', 'sate', '1mph', 'isod', 'gr4b', 'uagr', 'hiii', 'jabr', 'reba', 'temp', 'skul', 'scos', 'jlab'];
$field_map = array_merge($non_cat_fields, $cat_fields);
echo '$cat_fields: '; print_r($cat_fields); echo "\n";
echo '$field_map: '; print_r($field_map); echo "\n";;

$status_messages = $update_sql = [];
$count = 0;
foreach ($rows as $row) {
    if (++$count > 10) {
        #break;
    }
    printf("\n-----------------------Processing row %d of %d-----------------------" . PHP_EOL, $count + 1, count($rows));
    
    $new_user_cats = $user_cats;
    $mapped_row = [];
    $j = 0;
    $cats_to_remove = [];
    
    // just easier to read
    foreach ($row as $field) {
        $mapped_row[$field_map[$j]] = $field;
        $j++;
    }
    
    /**
     * yes means they can order these
     * no means they cannot order
     * the $user_cats_str are cats they cannot order from
     * so, if they have "Yes" in the field, we remove from prohibited list
     */
    $status_cats_removed_master = $status_cats_removed_regular = '';
    foreach ($mapped_row as $field => $field_val) {
        if (in_array($field, $cat_fields) && strtolower($field_val) == 'yes') {
            if (array_key_exists($field, $master_cat_ids)) {
                // master categories
                $status_cats_removed_master .= removeCatForMasterID($field);
            }
            
            // If this is in the regular category array, call other function
            if (array_key_exists($field, $cat_ids)) {
                $status_cats_removed_regular .= removeCatForSubcatID($field);
            }
        }
    }
    echo '$status_cats_removed_regular: '; print_r($status_cats_removed_regular); echo "\n";
    
    // reset array index
    $new_user_cats = array_values($new_user_cats);
    
    // status messages
    $status_messages[$mapped_row['account']] = [
        'accountname' => $mapped_row['accountname'], 
        'new_user_cats' => implode(',', $new_user_cats), 
        'status_cats_removed_master' => $status_cats_removed_master, 
        'status_cats_removed_regular' => $status_cats_removed_regular
    ];
    
    // collect update sql for is
    // get user_id for tessco_account
    $user_id = getUserIdFromTesscoAccount($mapped_row['account']);
    echo '$user_id: '; print_r($user_id); echo "\n";
    if ($user_id) {
        $update_sql[] = sprintf("UPDATE %s SET category_assignment = %s WHERE user_id = %d;", $table, implode(',', $new_user_cats), $user_id);
    }
    
    
}//foreach 

foreach ($status_messages as $account => $msg_arr) {
    printf("*** Updated cats for Account (%d), Account Name (%s)\n\t- Master cats removed: \n\t%s\n\t- Regular cats removed: \n\t%s\n\t- New User Categories:\n\t%s\n\n", $account, $msg_arr['accountname'], $msg_arr['status_cats_removed_master'], $msg_arr['status_cats_removed_regular'], $msg_arr['new_user_cats']);
}

echo "\n---------------------------------------------------------------------- UPDATE SQL -------------------------------------------------------------------------\n";
foreach ($update_sql as $sql) {
    printf("%s\n\n", $sql);
}
echo "\n\nDone.\n";

function getUserIdFromTesscoAccount($tessco_account)
{
    global $pdo;
    
    echo "SELECT user_id FROM users WHERE tessco_account = $tessco_account LIMIT 1\n";
    $stmt = $pdo->prepare('SELECT user_id FROM users WHERE tessco_account = ? LIMIT 1');
    $stmt->execute([$tessco_account]);
    $result = $stmt->fetch();
    
    return $result['user_id'] ?? false; 
    
}

/**
 * Remove from user cats. It's a single level array
  $cat_ids: Array
    [orgn] => 1334
 */
function removeCatForSubcatID($cat_name)
{
    global $cat_ids, $new_user_cats;
    
    echo "BEFORE COUNT: " . count($new_user_cats) . "\n";
    $cats_removed = '';
    echo '$cat_ids: '; print_r($cat_ids); echo "\n";
    echo '$cat_name: '; print_r($cat_name); echo "\n";
    echo '$cat_ids[$cat_name]: '; print_r($cat_ids[$cat_name]); echo "\n";
    
    $key = array_search($cat_ids[$cat_name], $new_user_cats);
    unset($new_user_cats[$key]);
    $cats_removed .= sprintf("%s (%d), ", $cat_name, $cat_ids[$cat_name]);
    
    echo "AFTER COUNT: " . count($new_user_cats) . "\n";
    
    return rtrim($cats_removed, ', ');
}

function removeCatForMasterID($master_cat_name) 
{
    global $all_cat_ids, $new_user_cats;
    
    $cats_removed = '';
    if (isset($all_cat_ids[$master_cat_name])) foreach ($all_cat_ids[$master_cat_name] as $cat_id_to_remove) {
        $key = array_search($cat_id_to_remove, $new_user_cats);
        unset($new_user_cats[$key]);
        $cats_removed .= sprintf("%s (%d), ", $master_cat_name, $cat_id_to_remove);
    }
    
    return rtrim($cats_removed, ', ');
}

/**
 * Query db to get all id's from vcomm_category for master_cat_id passed in
 * 
 * @param $master_category_id
 */
function getCatIdsForMasterCats($master_cats)
{
    global $pdo;
    
    $all_cat_ids = [];
    
    if ($master_cats) foreach ($master_cats as $field => $master_cat_id) {
        if (empty($master_cat_id)) {
            continue;
        }
        #echo "SELECT category_id FROM vcomm_category WHERE master_category_id = $master_cat_id\n";
        $stmt = $pdo->prepare('SELECT category_id FROM vcomm_category WHERE master_category_id = ?');
        $stmt->execute([$master_cat_id]);
        $result = $stmt->fetchAll();
        $all_cat_ids[$field][] = $master_cat_id;
        foreach ($result as $dbrow) {
            $all_cat_ids[$field][] = $dbrow['category_id'];
        }
        
    }
    
    return $all_cat_ids;
    
}




/**
 * @return PDO
 */
function getDBConnection()
{
    $host = '127.0.0.1';
    $db = 'myvoicecomm';
    $user = 'root';
    $pass = 'toor';
    $port = "3306";
    $charset = 'utf8mb4';
    
    $options = [
        \PDO::ATTR_ERRMODE => \PDO::ERRMODE_EXCEPTION,
        \PDO::ATTR_DEFAULT_FETCH_MODE => \PDO::FETCH_ASSOC,
        \PDO::ATTR_EMULATE_PREPARES => false,
    ];
    $dsn = "mysql:host=$host;dbname=$db;charset=$charset;port=$port";
    try {
        $pdo = new \PDO($dsn, $user, $pass, $options);
    } catch (\PDOException $e) {
        throw new \PDOException($e->getMessage(), (int)$e->getCode());
    }
    
    return $pdo;
}

/**
 * @param $label
 * @param $var
 */
function debug($label, $var)
{
    printf("%s : %s", $label, print_r($var, true));
    echo "\n";
}
