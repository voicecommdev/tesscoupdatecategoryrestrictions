<?php
error_reporting(E_ALL);


$table = 'myvoicecomm.users';

$user_cats_str = '175,258,295,362,384,397,407,422,423,425,446,447,463,464,479,502,521,544,578,616,622,628,634,640,643,644,656,659,669,676,687,705,706,710,714,715,730,731,735,736,737,751,752,767,773,774,775,791,796,797,798,803,804,807,808,810,812,813,814,816,818,819,820,827,829,830,831,832,836,837,838,839,840,841,844,850,851,861,865,866,867,868,870,872,873,875,876,879,881,882,885,900,901,911,912,913,921,922,927,930,931,932,934,940,941,943,944,945,946,947,949,950,956,959,960,963,964,968,969,970,975,980,981,983,988,991,992,993,995,997,1002,1003,1004,1005,1006,1007,1008,1009,1010,1011,1012,1013,1014,1016,1022,1023,1027,1032,1033,1034,1035,1043,1046,1047,1048,1051,1052,1053,1061,1062,1066,1067,1068,1069,1070,1072,1075,1078,1079,1083,1084,1087,1088,1089,1092,1093,1094,1095,1097,1098,1099,1100,1102,1103,1104,1105,1110,1115,1124,1125,1126,1127,1128,1129,1130,1131,1133,1134,1140,1147,1148,1149,1151,1152,1153,1154,1155,1157,1160,1164,1165,1167,1170,1171,1172,1173,1174,1180,1181,1182,1184,1185,1186,1195,1196,1200,1201,1202,1203,1204,1205,1207,1208,1209,1210,1211,1212,1213,1217,1219,1229,1231,1232,1233,1236,1238,1239,1240,1241,1242,1243,1244,1245,1248,1255,1256,1257,1258,1259,1261,1262,1263,1264,1265,1268,1269,1270,1272,1275,1276,1277,1278,1280,1281,1282,1283,1289,1290,1291,1292,1293,1294,1295,1297,1298,1299,1300,1306,1307,1308,1309,1310,1311,1312,1320,1321,1322,1323,1324,1325,1327,1328,1329,1330,1331,1332,1333,1334,1337,1339,1341,1342,1343,1344,1346,1349,1350,1351,1352,1354,1357,1358,1360,1362,1363,1364,1365,1366,1367,1368,1369,1370,1373,1374,1376,1379,1380,1381,1383,1384,1387,1388,1389,1391,1393,1394,1395,1396,1397,1402,1403,1404,1405,1406,1409,1410,1411,1412,1413,1414,1415,1416,1417,1418,1419';

// convert cats to array
$user_cats = explode(',', $user_cats_str);

$rows = array_map(function ($v) {
    return str_getcsv($v, "|");
}, file('RyanTesscoAccounts.csv'));

$header = array_shift($rows);

$status_messages = $update_sql = [];
$count = 0;
/*
 * 1st all - $cat_list_1
 * 2nd list if they have a Arp id. - $cat_list_2
 * 3rd 2nd and 3rd if they don't have the ArP id. - $cat_list_3
 */
$cat_list_1 = ['805', '806', '807', '808', '809', '810', '812', '813', '814', '815', '816', '817', '818', '819', '820', '821', '824', '825', '829', '855', '901', '927', '955', '980', '982', '985', '988', '1024', '1070', '1075', '1078', '1115', '1182', '1195', '1196', '1229', '1262', '1265', '1278', '1293', '1305', '1306', '1400', '1307', '1310', '1311', '1312', '1342', '1369'];
$cat_list_2 = ['1140', '911', '912', '913', '930', '931', '932', '1001', '1115'];
$cat_list_3 = ['464', '705', '706', '796', '819', '1067', '1087', '1185', '1280', '1328', '1344', '175', '362', '363', '364', '529', '551', '559', '577', '616', '617', '647', '684', '715', '735', '736', '737', '767', '797', '818', '879', '947', '995', '1012', '1061', '1068', '1079', '1105', '1110', '1151', '1172', '1232', '1244', '1320', '1322', '1337', '1343', '1362', '1363', '1364', '1365', '1376', '1389', '1391', '1395', '1404', '1388'];

$sql_file = 'update_users_table_tessco_category_restrictions_with_extra_cat.sql';
file_put_contents($sql_file, "");

$na_count = 0;
$cats_for_user_id = [];
$cat_id_list = [];
foreach ($rows as $row) {
    $arp_id = $row[0];
    $user_id = $row[1];
    if (++$count > 40) {
        #break;
    }
    printf("\n-----------------------Processing row %d of %d-----------------------" . PHP_EOL, $count + 1, count($rows));
    
    // for each row we need to see if we should add the $cat_list_[1-2-3] for that row
    
    
    /*
     * Per Justin: 
     *  - Everyone gets list 1.  
     *  - OtterID gets (1 and) 2
     *  - No Otter ID gets 1, 2, 3
     *  NOTE: this means EVERYONE gets 1 and 2, and no OtterID gets list 3 also.
     */
    // everyone gets list 1
    $cats_for_user_id[$row[1]] = $cat_list_1;
    
    // OtterID gets list 2 (and 1 since "everyone gets list 1")
    
    if ($row[0] != 'N/A') {
        $na_count++;
        $cats_for_user_id[$row[1]] = array_merge($cats_for_user_id[$row[1]], $cat_list_2);
    } else {
        // No OtterID gets 1, 2, 3
        $cats_for_user_id[$row[1]] = array_merge($cats_for_user_id[$row[1]], $cat_list_2);
        $cats_for_user_id[$row[1]] = array_merge($cats_for_user_id[$row[1]], $cat_list_3);
    }
    
    /**
     * yes means they can order these
     * no means they cannot order
     * the $user_cats_str are cats they cannot order from
     * so, if they have "Yes" in the field, we remove from prohibited list
     */
    
    $sql = sprintf("UPDATE %s SET category_assignment = '%s' WHERE user_id = %d LIMIT 1;", $table, implode(',', $cats_for_user_id[$row[1]]), $user_id);
    $cat_id_list[] = $user_id;
    
    $update_sql[] = $sql;
    file_put_contents($sql_file, $sql . "\n", FILE_APPEND);
}//foreach 

echo '$na_count: '; print_r($na_count); echo "\n";
foreach ($status_messages as $account => $msg_arr) {
    printf("*** Updated cats for Account (%d), Account Name (%s)\n\t- Master cats removed: \n\t%s\n\t- Regular cats removed: \n\t%s\n\t- New User Categories:\n\t%s\n\n", $account, $msg_arr['accountname'], $msg_arr['status_cats_removed_master'], $msg_arr['status_cats_removed_regular'], $msg_arr['new_user_cats']);
}


echo "\n---------------------------------------------------------------------- UPDATE SQL -------------------------------------------------------------------------\n";
foreach ($update_sql as $sql) {
    printf("%s\n\n", $sql);
}

echo "\n\nUSER IDS FOR SELECT IN \n";
echo "'" . implode("','", $cat_id_list) . "'\n";
echo "\n\nDone.\n";

